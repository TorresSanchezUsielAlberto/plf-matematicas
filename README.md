# MAPAS CONCEPTUALES
## Conjuntos, Aplicaciones y funciones (2002)
```plantuml
@startmindmap
caption Mapa Conceptual 1
<style>
mindmapDiagram {
    node {
        BackgroundColor #FFCB69
        LineColor black
    }
    rootNode {
        BackgroundColor #ACFF23
        FontColor black
        LineColor black
    }
    boxless {
    FontColor black
  }
    arrow {
    LineColor black
}
}
</style>
* Conjuntos, Aplicaciones y funciones
** Conjunto
*** conjunto y elemento
**** No se definen
****_ existe
*****: Una relacion de
pertenencia entre ellos;
******_ Esto quiere decir que
*******: Dado un elemento y un conjunto
 siempre se puede saber si 
 el elemento está o no 
 está dentro del conjunto;
 ****: De estas dos piezas
 elementales se construye 
 la teoria de conjuntos;
***: Inclusion de
conjuntos;
****: Un conjunto está 
incluido en otro conjunto;
*****_ Cuando
******: Todos los elementos 
del primer conjunto 
pertenecen al segundo;
***: Operaciones de
 conjuntos;
**** Interseccion
*****: Elementos que pertenecen
simultaneamente a ambos;
**** Union
*****: Elementos que pertenecen 
al menos a alguno de ellos;
**** Complementacion
*****: Los que no pertenecen complementario 
de un conjunto que no pertenece 
a un conjunto dado;
***: Tipos de
conjuntos;
**** Universal
***** Es un conjunto de referencia
******_ En el cual
******* Ocurren todas las cosas de la teoría
**** Vacio
***** Es una necesidad lógica
******_ Para
******* Cerrar bien las cosas
*****: Es el conjunto que
 no tiene elementos;
*****: El cardinal del conjunto
 vacío es cero;
*****: En particular, el conjunto
 vacío es un conjunto finito;
***: Diagramas
de Venn;
****: Son esquemas usados
 en la teoría de conjuntos;
*****_ Ideados por
****** John Venn
******_ En
******* 1880
****_ Sirven para 
***** Representar conjuntos
******: Ayudan a comprender gráficamente 
las operaciones de conjuntos;
***: Cardinal de 
un conjunto;
****_ ¿Que es?
*****: Es el número de elementos
que conforman el conjunto;
****** Es un numero natural
**** Formulas
*****: Formula del cardinal de
 la unión de conjuntos;
****** El cardinal de una unión 
*******_ Es igual 
********: Al cardinal
 de uno de los conjuntos;
*********_ Más 
**********: El cardinal del
segundo conjunto;
***********_ Menos 
************: El cardinal de la 
intersección de los elementos comunes;
** Aplicaciones y funciones
*** Aplicacion matematica
****_ ¿Que es?
*****: Es una transformación 
o una regla;
****** ¿Que hace?
*******: Convierte a cada uno de los elementos 
de un determinado conjunto inicial;
********_ En
*********: Un único elemento 
de un conjunto final;
**** Tipos de aplicacion
*****_ Sirven para
******: Dar información acerca de 
cómo se relacionan los elementos;
*******_ De
******** Un conjunto inicial X 
*********_ Con 
********** Un conjunto final Y
***** Inyectiva
****** Si cada elemento del conjunto final Y 
*******_ Tiene 
********: Un único elemento del 
conjunto inicial X 
al que le corresponde;
***** Sobreyectiva
******: Todos los elementos 
del conjunto final Y;
*******_ Tienen al menos 
********: Un elemento del conjunto 
inicial X al que le corresponde;
***** Biyectiva
******_ Es
******* Una funcion
********_ Que
*********: Al mismo tiempo es 
Inyectiva y Sobreyectiva;
****: La idea de transformación
 de transformación;
*****: Nos conduce a hacer una aplicación primero
 y otra aplicación a continuación al transformado
  mediante la primera aplicación;
******_ Se le llama
******* La composición de aplicaciones
*** Funcion
****: Son aplicaciones que como tienen
 el sustrato de los números 
 son más fáciles de ver;
****: Los conjuntos que se transforman
 son conjuntos de números;
*****_ Los numeros pueden ser
****** Números naturales
****** Números enteros
****** Números fraccionarios
****** Números reales
center footer Torres Sanchez Usiel Alberto
@endmindmap
```
## Funciones (2010)
```plantuml
@startmindmap
caption Mapa Conceptual 2
<style>
mindmapDiagram {
    node {
        BackgroundColor #FFCB69
        LineColor black
    }
    rootNode {
        BackgroundColor #ACFF23
        FontColor black
        LineColor black
    }
    boxless {
    FontColor black
  }
    arrow {
    LineColor black
}
}
</style>
* Funciones
** ¿Que es?
*** Es una aplicación especial 
****_ En la que
*****: Los conjuntos que se están 
relacionando son conjuntos de números;
******_ En particular
******* Conjuntos de numeros reales
*** Se pueden representar graficamente
****_ Mediante
***** La representacion cartesiana
******: Se introdujo en honor a 
René Descartes;
****** Es un sistema bidimensional
******* Tiene 2 ejes
******** El eje de las abscisas
*********_ Tambien llamado
********** Eje de las "x"
******** El eje de las ordenadas
*********_ Tambien llamado
********** Eje de las "y"
******* Se denomina plano cartesiano
** Caracteristicas
*** Funciones crecientes
**** Cuando aumenta la variable independiente
*****_ Entonces
****** Aumentan sus imagenes
****: En la representacion grafica
la grafica se eleva;
*** Funciones decrecientes
**** Cuando aumenta la variable independiente
*****_ Entonces
****** Van disminuyendo sus imagenes
****: En la representacion grafica
la grafica va disminuyendo;
*** Expresiones intuitivas
**** Maximos
*****: Cuando la función crece hasta un
 determinado valor y a partir de ese 
 valor la función comience a decrecer;
***** Maximo absoluto
******: Es el valor más grande
 en todo el dominio;
**** Minimos
*****: Cuando la función decrece hasta un 
determinado valor y a partir de ese 
valor la función comience a crecer;
***** Minimo absoluto
******: Es el valor más pequeño 
en todo el dominio;
** Conceptos importantes
*** Limite
****: El límite es el valor al cuál
 nos acercamos según el 
 punto de arranque;
*** Continuidad
**** Son funciones más manejables
**** Es una función que tiene buen comportamiento
***** Es una funcion que no produce saltos
***** Es una funcion que no produce discontinuidades
*** Derivada
****: Resuelve el problema de la 
aproximación de una función compleja;
*****_ Mediante
****** Una función lineal (función simple)
**** Es la pendiente de una recta tangente
center footer Torres Sanchez Usiel Alberto
@endmindmap
```
## La matemática del computador (2002)
```plantuml
@startmindmap
caption Mapa Conceptual 3
<style>
mindmapDiagram {
    node {
        BackgroundColor #FFCB69
        LineColor black
    }
    rootNode {
        BackgroundColor #ACFF23
        FontColor black
        LineColor black
    }
    boxless {
    FontColor black
  }
    arrow {
    LineColor black
}
}
</style>
*: La matematica
del computador;
**: Aritmetica del
computador;
*** Numero aproximado
**** Son numeros
*****_ Donde
******: Su valor no es exactamente 
el que corresponde a la cifra dada;
*** Error
****_ Pueden ser
***** Error absoluto
***** Error relativo
*** Digito Significativo
**** Son numeros
*****_ Que
****** Proporcionan informacion
*******_ Sobre
******** La magnitud del signo
***: Aspectos matematicos 
de la aritmetica finita;
**** Truncamiento de un numero
*****_ Consiste 
******: En cortar unas cuantas
 cifras a la derecha;
*******_ A partir de 
******** Una cifra dada
*****_ Un ejemplo
******: El truncamiento del 
numero pi seria 3.1415;
**** Redondear un numero
*****_ Consiste 
******: En despreciar un determinado
 número de cifras;
*******_ Y
********: Retocar la última cifra que
 no despreciamos de un modo adecuado;
*********_ Un ejemplo
********** El redondeo del numero pi seria 3.1416
*****: Es una especie de 
truncamiento refinado;
*****_ Evita
******: Que sean menos los
errores cometidos;
**  Sistemas
*** Sistema Binario
**** Es el sistema mas simple
**** Se ocupa la base 2
*****_ Esto quiere decir
****** Solo se ocupan dos digitos
*******_ Con los cuales
******** Todo un sistema de numeracion
*********_ los numeros son
********** 0
********** 1
****_ Se utiliza en
***** Videos
***** Imagenes
***** Digitalizacion
***** Aspectos diigitales
****_ Enlaza con
***** La logica booleana
***** Conjuntos
****** La pertenencia
*******_ Y la
******** No pertenencia
**** En la informatica
*****_ Porque
******: Todo está en el mundo
 de la electrónica;
******* Es como un interruptor
******** Cuando se enciende
*********:El 1 se puede identificar 
con el paso de corriente;
******** Cuando se apaga
*********:El 0 se puede identificar
 con el no paso de corriente;
********:Con esta idea se pueden
 armar estructuras complejas;
**** Adicion binaria
***** Es la suma de numeros binarios
**** Representacion interna de numeros
*****_ ¿Que es?
****** Ideas matematicas
*******_ Que
********: Nos enseñan como
 convertir ceros y unos;
*********_ En
**********: Una serie de posiciones 
de memoria de un ordenador;
***********_ Para
************ Convertir eso en un numero entero
*****_ ¿Cuales son?
****** La representación en magnitud signo
****** La representación en exceso
****** La representación en complemento 2
*** Sistema Octal
**** Es en base 8
*****: Tiene más símbolos, pero se utilizan menos
 para representar un numero
  decimal que en el sistema binario;
*** Sistema Hexadecimal
**** Es en base 16
*****: Tiene más símbolos, pero se utilizan menos
 para representar un numero
  decimal que en el sistema binario;
  center footer Torres Sanchez Usiel Alberto
@endmindmap
```